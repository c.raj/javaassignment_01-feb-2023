package com.book;

public class NoteBook extends Book {

	@Override
	public void write() {
		// TODO Auto-generated method stub
		System.out.println("im writing");
	}

	@Override
	public void read() {
		System.out.println("im reading");
	}
	
	public void draw() {
		
		System.out.println("im drawing");
	}
	
	public static void main(String[] args) {
		
		NoteBook nb = new NoteBook();
		nb.write();
		nb.read();
		nb.draw();
	}

}
