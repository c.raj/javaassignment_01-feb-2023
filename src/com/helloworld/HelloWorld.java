package com.helloworld;

public class HelloWorld {
	
	public static void main(String[] args) {
	    String string = "Hello World";
	    int firstletter = string.indexOf("o");
	    int lastletter = string.lastIndexOf("o");
	    System.out.println("The First occurence 'o' letter is located at " + firstletter);
	    System.out.println("The Last occurance 'o' letter is located at " + lastletter);
	  }

}
