package com.shape;

public class Area extends Shape {

	@Override
	public void rectangleArea(int length, int breadth) {
		// TODO Auto-generated method stub
		int rectarea = length * breadth;
		System.out.println("area of rectangle is "+rectarea);
		
	}

	@Override
	public void squareArea(int side) {
		// TODO Auto-generated method stub
		int squarea = side*side;
		System.out.println("area of square is "+squarea);
		
		
	}

	@Override
	public void circleArea(int radius) {
		// TODO Auto-generated method stub
		double pi =Math.PI,circarea;
		circarea = pi*radius*radius;
		System.out.println("area of circle is "+circarea);
		
	}

	public static void main(String[] args) {
		Area a = new Area();
		a.rectangleArea(15, 30);
		a.squareArea(6);
		a.circleArea(7);
	}
	
}
