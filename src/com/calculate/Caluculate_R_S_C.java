package com.calculate;

import java.util.Scanner;

public class Caluculate_R_S_C {

	public static void main(String[] args) {

		System.out.print("Enter a rectangle width in float: ");
		Scanner sc = new Scanner(System.in);

		double rectanglewidth = sc.nextFloat();
		System.out.print("Enter a rectangle height in float: ");
		double rectangleheight = sc.nextFloat();
		
		double rectanglearea = rectanglewidth * rectangleheight;
		System.out.println("Area of rectangle: " + rectanglearea);

		System.out.print("Enter a square side in float: ");
		double squareside = sc.nextFloat();
		
		double square_area = squareside * squareside;
		System.out.println("Area of square: " + square_area);

		System.out.print("Enter a radious of circle in float: ");
		double circleradius = sc.nextFloat();
		
		double circlearea = Math.PI * circleradius * circleradius;
		System.out.println("Area of circle: " + circlearea);
	}

}
