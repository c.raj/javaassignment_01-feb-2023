package com.products;

import java.util.LinkedList;

public class ProductsLinkedList {
	
	public static void main(String[] args) {
		
		LinkedList<String> list = new LinkedList<String>();
		
		list.add("Nescafe");
		list.add("Kinley");
		list.add("Coke");
		list.add("Parachute");
		list.add("Iphone");
		System.out.println(list.size());
		System.out.println(list);
		list.remove(1);
		System.out.println(list);
		
		boolean b =list.contains("Coke");
		System.out.println("Coke is present "+b);
	}

}
